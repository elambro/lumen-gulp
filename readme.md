# Lumen - Gulp - Bootstrap - SCSS

A quick setup for small web applications

## Official Documentation

- [Lumen](http://lumen.laravel.com/docs)
- [Gulp](http://gulpjs.com/)
- [Bootstrap](http://getbootstrap.com/)
- [jQuery](https://jquery.com/)
- [Sass](http://sass-lang.com/)
- [Dropzone]()