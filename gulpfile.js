/* Require all the dependencies */
var gulp = require("gulp");
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
// var browserify = require('browserify');
var clean = require('gulp-clean');
var concat = require('gulp-concat');
var merge = require('merge-stream');
var imagemin = require('gulp-imagemin');
var minify = require('gulp-minify');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var newer = require('gulp-newer');

var SOURCEPATHS = {
    sassSource: 'resources/assets/scss/*.scss',
    jsSource: 'resources/assets/js/**',
    imgSource: 'resources/assets/img/**'
};
var APPPATH = {
    root: 'public/',
    css:  'public/css',
    js:   'public/js',
    fonts: 'public/fonts',
    img: 'public/img'
};

/* Remove any files that are deleted in the src javascript */
gulp.task('clean-scripts', function () {
    return gulp.src(APPPATH.js + '/*.js', {read:false, force: true}) // removes files that dont exist
        .pipe(clean());
    });

/* Create a task with the command 'sass'. "$ gulp sass " to run this command */
gulp.task('sass', function() {

    var bootstrapCSS = gulp.src('./node_modules/bootstrap/dist/css/bootstrap.css'); // Get boostrap.css from node
    var dropzoneCSS = gulp.src('./node_modules/dropzone/dist/min/dropzone.min.css');
    var sassFiles;

    sassFiles = gulp.src(SOURCEPATHS.sassSource)                    // gulp.src('src/scss/app.scss')
        .pipe(autoprefixer())                                   // automatically add vendor prefixer
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError));  // compile the sass to css

        return merge(bootstrapCSS, dropzoneCSS, sassFiles)               /* Append our SASS to the bootstrap file */
        .pipe(concat('app.css'))                    // outputStyle can be 'compact', 'nested', 'compressed', 'expanded'

        .pipe(gulp.dest(APPPATH.css));               // .pipe(gulp.dest('public/css')); -- Export to the destination
});

/* Copy and minify images */
gulp.task('images', function () {
    return gulp.src(SOURCEPATHS.imgSource)
        .pipe(newer(APPPATH.img))           // Will find any new images and copy them into the public dir
        .pipe(imagemin())                   // Minifies the image
        .pipe(gulp.dest(APPPATH.img));
    });

/* Move fonts from node_modules into our public dir */
gulp.task('moveFonts', function () {
    gulp.src('./node_modules/bootstrap/fonts/*.{eot,svg,ttf,woff,woff2}')
    .pipe(gulp.dest(APPPATH.fonts));
    });


/* Create a task for scripts that concats all javascript and puts it in the public dir */
gulp.task('scripts', ['clean-scripts'], function () {

    var bootstrapJS = gulp.src('./node_modules/bootstrap/dist/js/bootstrap.min.js');
    var dropzoneJS = gulp.src('./node_modules/dropzone/dist/dropzone.js');
    var jQuery = gulp.src('./node_modules/jquery/dist/jquery.min.js');
    var appJS = gulp.src(SOURCEPATHS.jsSource);

    return merge(jQuery, bootstrapJS, dropzoneJS, appJS)
    .pipe(concat('main.js'))
    // .pipe(browserify())
    .pipe(gulp.dest(APPPATH.js));
    });

/*-----------------------------------------------------*/
/** Production Tasks */

gulp.task('compress', function () {
    gulp.src(SOURCEPATHS.jsSource)
    .pipe(concat('main.js'))
    // .pipe(browserify())
    .pipe(minify())
    .pipe(gulp.dest(APPPATH.js));
    });

gulp.task('compresscss', function() {

    var bootstrapCSS = gulp.src('./node_modules/bootstrap/dist/css/bootstrap.css'); // Get boostrap.css from node
    var sassFiles;

    sassFiles = gulp.src(SOURCEPATHS.sassSource)                    // gulp.src('src/scss/app.scss')
        .pipe(autoprefixer())                                   // automatically add vendor prefixers
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError));  // compile the sass to css

        return merge(bootstrapCSS, sassFiles)               /* Append our SASS to the bootstrap file */
        .pipe(concat('app.css'))
        .pipe(cssmin())
            .pipe(rename({suffix: '.min'}))

        // outputStyle can be 'compact', 'nested', 'compressed', 'expanded'

        .pipe(gulp.dest(APPPATH.css));             // .pipe(gulp.dest('public/css')); -- Export to the destination
});
/** End of production tasks */
/*-----------------------------------------------------*/


/* When I type 'serve' into the console, run the following tasks */
gulp.task('serve', ['sass', 'scripts', 'clean-scripts', 'moveFonts', 'images'], function () {         // Compile sass, then sync
    gulp.watch(SOURCEPATHS.sassSource, ['sass']);
//  gulp.watch(SOURCEPATHS.htmlSource, ['copy']);
    gulp.watch(SOURCEPATHS.jsSource, ['scripts']);
});

gulp.task('default', ['serve']); 
gulp.task('production', ['compresscss', 'compress']);